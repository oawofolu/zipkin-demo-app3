package io.vmware.pubsec.distributedtracing.app3;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

@SpringBootApplication
public class App3 {

	public static void main(String[] args) {
		SpringApplication.run(App3.class, args);
	}
	
	@Bean RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

}

@RestController
class HelloWorldApp3 {
	private Logger LOG = LoggerFactory.getLogger(getClass());
	@Autowired RestTemplate restTemplate;
	
	@GetMapping
	public String parent(HttpServletRequest request) {
		UriComponents path = ServletUriComponentsBuilder.fromServletMapping(request).path("/child").build();
		LOG.info("In parent API...navigating to {}");
		return restTemplate.getForObject(path.toString(), String.class);
	}
	
	@GetMapping("/child")
	public String child() {
		LOG.info("In child API...");
		return "Hello World! From child";
	}
}
